# Description

App for predicting user ratings based on a movie description

# Data retrieval

In the model/data_retrieval you can find a script that received data from the resource

jupiter notebook - [data_retrieval.ipynb](https://gitlab.com/iKzN/predict-film-rating-votes-by-overview/-/blob/master/model/data_retrieval/data_retrieval.ipynb)

# Research

In the model/research:
1) report - [report_predict_film_rating_votes_by_overview.pdf](https://gitlab.com/iKzN/predict-film-rating-votes-by-overview/-/tree/master/model/research/report_predict_film_rating_votes_by_overview.pdf)
2) jupiter notebook - [predict_film_rating_votes_by_overview.ipynb](https://gitlab.com/iKzN/predict-film-rating-votes-by-overview/-/tree/master/model/research/predict_film_rating_votes_by_overview.ipynb)
3) csv files with metrics value

# Run app

### Build container
docker-compose up --build

# Work with started app (local)

### Interface
http://localhost:5001/ - url with user interface

Example 1:

![Example 1](/static/images/Predict Film Rating Votes By Overview — Example 1.jpg)

Example 2 (change the phrase):

![Example 2](/static/images/Predict Film Rating Votes By Overview — Example 2.jpg)

### Url for api (local)
http://localhost:5001/api/get_rating
