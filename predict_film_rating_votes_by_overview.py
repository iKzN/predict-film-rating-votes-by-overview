import os
import joblib
import numpy as np
import pandas as pd
import json
import re

from flask import Flask, request, flash, jsonify, abort, redirect, url_for, render_template, send_file, after_this_request
from flask_wtf import FlaskForm
from markupsafe import escape
from wtforms import StringField, FileField
from wtforms.validators import DataRequired
from werkzeug.utils import secure_filename

import tensorflow as tf
from tensorflow import keras
import pickle
from keras.preprocessing.sequence import pad_sequences

app = Flask(__name__)

# config
app.config.from_object('config.DevConfig')

def build_model(vocab_size, embedding_dim):
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Embedding(vocab_size, embedding_dim, input_length=embedding_dim))
    model.add((tf.keras.layers.LSTM(100,return_sequences=True)))
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(100, activation='relu'))
    model.add(tf.keras.layers.Dropout(0.2))
    model.add(tf.keras.layers.Dense(1))
    model.compile(loss='mse', optimizer='adam', metrics=['mse'])
    return model

# create model
generation_model = build_model(
    vocab_size = 10000,
    embedding_dim=200)

generation_model.load_weights("model/test_model2")
generation_model.build(tf.TensorShape([1, None]))

# loading tokenizer
with open('model/tokenizer.pickle', 'rb') as handle:
    tokenizer = pickle.load(handle)

# form
@app.route('/')
def clear_format():
    return render_template('text_form.html')

# path to request
@app.route('/get_rating', methods = ['POST'])
def load_paint():
    """Return answer"""
    try:
        content = request.get_json()
        predict = generate_text(generation_model, string=text_clear(content['input_text']))
    except Exception as e:
        print('error')
        print(e)
        return redirect(url_for('bad_request'))

    print(predict)
    return jsonify(predict)

# error page
@app.route('/badrequest')
def bad_request():
    return abort(400)

# additional func

def text_clear(text):
    result = text.lower()
    result = re.sub(r'[!?]', '.', result)
    result = re.sub(r'[^а-яa-z\.\-\,]', ' ', result)
    result = re.sub(r"[\|/]", '', result) 
    result = re.sub(r"[\n]", ' ', result) 
    result = re.sub(r"[\r]", ' ', result) 
    result = re.sub(r"[nan]", ' ', result) 
    result = re.sub(r'\s+', ' ', result)
    result = re.sub(r' \.', '.', result)
    result = re.sub(r'\. \.', '.', result)
    result = re.sub(r'\.\.', '.', result)
    return result

def generate_text(model, string):
    seq = tokenizer.texts_to_sequences([string])
    padded = pad_sequences(seq, maxlen=200)
    pred1 = generation_model.predict(padded)
    return str(pred1[0][0])
